set nomodeline
set noswapfile
set encoding=UTF-8
set ttyfast
set notermguicolors
set wildmenu
set wildignore+=*/tmp/*,*.so,*.swp,*.zip,*/.tmp/*,*/.sass-cache/*,*/node_modules/*,*.keep,*.DS_Store,*/.git/*,*/__pychache__/*,*/zig-cache/*,*/.idea/*,*/.vscode/*
set mouse=a
" # http://superuser.com/questions/549930/cant-resize-vim-splits-inside-tmux
if &term =~ '^screen'
  " tmux knows the extended mouse mode
  set ttymouse=xterm2
endif

set path+=**

" Whitespace
set nowrap                      " don't wrap lines
set tabstop=2 shiftwidth=2      " a tab is two spaces (or set this to 2)
set expandtab                   " use spaces, not tabs (optional)
set backspace=indent,eol,start  " backspace through everything in insert mode

set splitright
set splitbelow

" Use the OS clipboard by default
set clipboard^=unnamed
set nocursorline
set showbreak=↪\

" Statusline
set laststatus=2
set statusline=%f\ %=\ %y\ %l:%c

" Searching
set hlsearch                    " highlight matches
set incsearch                   " incremental searching

" set backup       " keep a backup file
set backupcopy=yes
set undofile
set undodir=~/.vim/undo//

set history=50    " keep 50 lines of command line history
set title " terminal title
set autoread " load change files

" Clears the search register
nmap <silent> <leader>cs :nohlsearch<CR>
" Edit the vimrc file
nnoremap <silent> <leader>ev :e $MYVIMRC<CR>
nnoremap <silent> <leader>rv :so $MYVIMRC<CR>

nmap <silent> <leader>cc :make<cr>
nmap <silent> <leader>af <C-^>
nmap <silent> <leader>wf :w<cr>

set shortmess+=c
set omnifunc=syntaxcomplete#Complete

syntax enable
filetype plugin indent on    " required
filetype plugin on

" builtin plugins
packadd! matchit
packadd! editorconfig
packadd! comment

" Line Numbers
set number
set relativenumber
function! ShowLinesFunc()
  set number
  set relativenumber
endfunction
function! HideLinesFunc()
  set nonumber
  set norelativenumber
endfunction
command! -nargs=0 ShowLines :call ShowLinesFunc()
command! -nargs=0 HideLines :call HideLinesFunc()

" Syntax Highlighting and File Types
autocmd! FileType c setlocal tabstop=4 shiftwidth=4 noexpandtab commentstring=//\ %s
autocmd! FileType cpp setlocal tabstop=4 shiftwidth=4 noexpandtab commentstring=//\ %s

" Remove trailing whitespace on save
autocmd BufWritePre * %s/\s\+$//e

" Spell Checking
autocmd Filetype gitcommit setlocal spell
autocmd Filetype markdown setlocal spell

" Folding
set foldmethod=expr

" Themes
let g:current_theme='dark'

function! LoadLight()
  let g:current_theme='light'
  set background=light
  "colo retrobox
endfunction

function! LoadDark()
  let g:current_theme='dark'
  set background=dark
  "colo retrobox
endfunction

function! ToggleTheme()
  if g:current_theme == 'light'
    call LoadDark()
  elseif g:current_theme == 'dark'
    call LoadLight()
  endif
endfunction

" GUI Settings
set guifont=CommitMono:h15
set guioptions-=l
set guioptions-=r
set guioptions-=T
set guioptions-=R
set guioptions-=m
set guioptions-=L

" Use Ag
set grepprg=ag\ --vimgrep

if g:current_theme == 'light'
  call LoadLight()
elseif g:current_theme == 'dark'
  call LoadDark()
endif

function! RailsTestFunc(path)
  let &makeprg='bin/rails t ' .. a:path
  make
endfunction
command! -nargs=1 RailsTest :call RailsTestFunc(<f-args>)

function! PrettierFormatFunc(path)
  let &makeprg='npx prettier -w ' .. a:path
  make
endfunction
command! -nargs=1 PrettierFormat :call PrettierFormatFunc(<f-args>)

function! TSCompFunc()
  let &makeprg='npx tsc -p tsconfig.json'
  make
endfunction
command! -nargs=0 TSComp :call TSCompFunc()

