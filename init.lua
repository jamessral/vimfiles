-- Options
vim.opt.path:append('**')
vim.opt.wildmenu = true
vim.opt.wildignore:append(
    '*/tmp/*,*.so,*.swp,*.zip,*/.tmp/*,*/.sass-cache/*,*/node_modules/*,*.keep,*.DS_Store,*/.git/*,*/__pychache__/*')
vim.opt.mouse = 'a'

-- matching brace/parenthesis/etc.
vim.g.loaded_matchparen = 1
vim.opt.hidden = true

-- GUI Settings
vim.opt.guifont = 'CommitMono:h16'
vim.opt.guicursor = ''
vim.opt.linespace = 3
-- Whitespace
vim.opt.tabstop = 4
vim.opt.shiftwidth = 2                 -- a tab is four spaces (or vim.opt.this to 4)
vim.opt.expandtab = true               -- use spaces, not tabs (optional)
vim.opt.backspace = 'indent,eol,start' -- backspace through everything in insert mode
vim.opt.signcolumn = 'yes'
vim.opt.modelines = 0

-- Vertical split to right
vim.opt.splitright = true

-- Horizontal split below
vim.opt.splitbelow = true

-- Use the OS clipboard by default
vim.opt.clipboard = 'unnamedplus'

-- Statusline
vim.opt.laststatus = 2
vim.opt.statusline = '%f%m%=%l:%c %y'

vim.opt.showbreak = '↪\\'

-- Searching
vim.opt.hlsearch = true   -- highlight matches
vim.opt.incsearch = true  -- incremental searching
vim.opt.ignorecase = true -- searches are case insensitive...
vim.opt.smartcase = true  -- ... unless they contain at least one capital letter
vim.opt.backupcopy = 'yes'
vim.opt.undofile = true

vim.opt.history = 50 --   keep 50 lines of command line history
vim.opt.ruler = true -- show the cursor position all the time
vim.opt.title = true
vim.opt.autoread = true
vim.opt.shortmess:append('c')

vim.opt.relativenumber = true
vim.opt.number = true

vim.cmd [[ filetype plugin indent on ]]
vim.cmd [[ filetype on ]]
vim.cmd [[ set omnifunc=syntaxcomplete#Complete ]]
-- UI

Current_theme = 'dark'
function LoadDark()
    Current_theme = 'dark'
    vim.cmd.set('background=dark')
    vim.cmd("colo quiet")
end

function LoadLight()
    Current_theme = 'light'
    vim.cmd.set('background=light')
    vim.cmd("colo quiet")
end

function SwitchTheme()
    if Current_theme == 'light' then
        LoadDark()
    elseif Current_theme == 'dark' then
        LoadLight()
    end
end

vim.g.mapleader=' '
-- Mappings
vim.api.nvim_set_keymap('i', '<C-c>', '<Esc>', { noremap = true })
vim.api.nvim_set_keymap('x', '<C-c>', '<Esc>', { noremap = true })

vim.api.nvim_set_keymap('n', '<leader>cs', ':nohlsearch<CR>', { silent = true })
vim.api.nvim_set_keymap('n', '<leader>wf', ':w<CR>', { silent = true })
vim.api.nvim_set_keymap('n', '<leader>af', '<C-^>', { silent = true })
vim.api.nvim_set_keymap('n', '<leader>cc', ':make<cr>', { silent = true })
vim.api.nvim_set_keymap('n', '<leader>gs', ':Git<cr>', { silent = true })

vim.api.nvim_set_keymap('n', '<leader>f', ':Telescope find_files<cr>', { noremap = true, silent = true })
vim.api.nvim_set_keymap('n', '<leader>b', ':Telescope buffers<cr>', { noremap = true, silent = true })
vim.api.nvim_set_keymap('n', '<leader>s', ':Telescope live_grep<cr>', { noremap = true, silent = true })
vim.api.nvim_set_keymap('n', '<leader>tt', ':TestNearest<cr>', { silent = true })
vim.api.nvim_set_keymap('n', '<leader>tf', ':TestFile<cr>', { silent = true })

vim.api.nvim_set_keymap('n', '<leader>ev', ':e $MYVIMRC<CR>', { noremap = true, silent = true })
vim.api.nvim_set_keymap('n', '<leader>rv', ':so $MYVIMRC<CR>', { noremap = true, silent = true })

vim.cmd [[ let test#strategy = "neovim" ]]

-- UI
vim.api.nvim_set_keymap('n', '<F5>', ':lua SwitchTheme()<cr>', { noremap = true, silent = true })

-- Use Ag
vim.opt.grepprg = 'ag --vimgrep'

-- Filetype defaults
vim.cmd [[
autocmd! FileType lua setlocal tabstop=4 shiftwidth=4
autocmd! FileType c setlocal tabstop=4 shiftwidth=4 noexpandtab commentstring=//\ %s
autocmd! FileType cpp setlocal tabstop=4 shiftwidth=4 noexpandtab commentstring=//\ %s
autocmd! FileType odin setlocal tabstop=4 shiftwidth=4 noexpandtab commentstring=//\ %s
autocmd! FileType odin let &errorformat="%f(%l:%c) %m"
autocmd! FileType zig setlocal tabstop=4 shiftwidth=4 noexpandtab commentstring=//\ %s
autocmd! FileType go setlocal tabstop=4 shiftwidth=4 noexpandtab commentstring=//\ %s
autocmd! BufNewFile,BufRead *.cpp setlocal tabstop=4 shiftwidth=4
autocmd! BufNewFile,BufRead *.c setlocal tabstop=4 shiftwidth=4
autocmd! BufNewFile,BufRead *.odin setlocal tabstop=4 shiftwidth=4
autocmd! BufNewFile,BufRead *.zig setlocal tabstop=4 shiftwidth=4
autocmd! BufNewFile,BufRead *.go setlocal tabstop=4 shiftwidth=4
autocmd! BufNewFile,BufRead *.js setlocal tabstop=2 shiftwidth=2
autocmd! BufNewFile,BufRead *.jsx setlocal tabstop=2 shiftwidth=2
autocmd! BufWritePre * %s/\s\+$//e
]]

-- Packages
-- Bootstrap Lazy
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
    vim.fn.system({
        "git",
        "clone",
        "--filter=blob:none",
        "https://github.com/folke/lazy.nvim.git",
        "--branch=stable", -- latest stable release
        lazypath,
    })
end
vim.opt.rtp:prepend(lazypath)
require("lazy").setup({
    {
        'nvim-telescope/telescope.nvim',
        branch = '0.1.x',
        dependencies = { 'nvim-lua/plenary.nvim' },
        config = function()
            require('telescope').setup({
                defaults = { color_devicons = false },
            })
        end,
    },
    {
        'rose-pine/neovim',
        name = 'rose-pine',
    },
    "sheerun/vim-polyglot",
    "tpope/vim-fugitive",
    "vim-test/vim-test",
})

if Current_theme == 'light' then
    LoadLight()
else
    LoadDark()
end
